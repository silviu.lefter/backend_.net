﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_.NET.DataLayer.Entities
{
    public class FacultyStudent
    {
        public int StudentId { get; set; }
        public int FacultyId { get; set; }

        public virtual Faculty Faculty { get; set; }
        public virtual Student Student { get; set; }
    }
}
