﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_.NET.DataLayer.Entities
{
    public class Faculty
    {

        [Key]
        public int FacultyId { get; set; }

        public string Name { get; set; }

        public List<FacultyStudent> FacultyStudents { get; set; }

/*        public int StudentId { get; set; }*/
    }
}
