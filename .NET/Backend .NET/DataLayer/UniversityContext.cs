﻿using Backend_.NET.DataLayer.Entities;
using Backend_.NET.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_.NET.DataLayer
{
    public class UniversityContext : DbContext
    {
        public UniversityContext(DbContextOptions options) : base(options)
        {

        }

        public virtual DbSet<Student> Students { get; set; }

        public virtual DbSet<Faculty> Faculties { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FacultyStudent>()
                .HasKey(bc => new { bc.StudentId, bc.FacultyId });
            modelBuilder.Entity<FacultyStudent>()
                .HasOne(bc => bc.Student)
                .WithMany(b => b.FacultyStudents)
                .HasForeignKey(bc => bc.StudentId);
            modelBuilder.Entity<FacultyStudent>()
                .HasOne(bc => bc.Faculty)
                .WithMany(c => c.FacultyStudents)
                .HasForeignKey(bc => bc.FacultyId);
        }
    }
}
