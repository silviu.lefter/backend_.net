﻿using Backend_.NET.DataLayer;
using Backend_.NET.DataLayer.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_.NET.Controller
{
    [Route("api/faculties")]
    public class FacultyController : ControllerBase
    {
        private readonly UniversityContext _db;
        public FacultyController(UniversityContext db)
        {
            _db = db;
        }

        [HttpPost]
        public IActionResult CreateFaculty([FromBody] Faculty newFaculty)
        {
            var toAdd = new Faculty
            {
                Name = newFaculty.Name
            };

            _db.Add(toAdd);
            _db.SaveChanges();

            return Ok();
        }
    }
}
