﻿using Backend_.NET.DataLayer;
using Backend_.NET.DataLayer.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_.NET.Controller
{
    [Route("api/students")]
    public class StudentController : ControllerBase
    {
        private readonly UniversityContext _db;

        public StudentController(UniversityContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult GetAll(int id)
        {
            var employee = _db.Students;

            if (employee == null)
                return BadRequest();
            else
                return Ok(employee);
        }

        [HttpPost]
        public IActionResult CreateNew([FromBody] Student newStudent)
        {
            var toAdd = new Student
            {
                FirstName = newStudent.FirstName,
                LastName = newStudent.LastName
            };

            _db.Add(toAdd);

            _db.SaveChanges();

            //EmployeesRepository.AddNew(newEmployee);

            return Ok();
        }
    }
}
