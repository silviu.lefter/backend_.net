﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_.NET.Model
{
    public class StudentModel
    {
        public int Id { get; set; }
        public int FirstName { get; set; }

        public int LastName { get; set; }

        public List<FacultyModel> Faculties { get; set; }
    }
}
