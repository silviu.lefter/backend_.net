﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_.NET.Model
{
    public class FacultyModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<StudentModel> Students { get; set; }
    }
}
